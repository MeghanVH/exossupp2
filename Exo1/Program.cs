﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Exo1_1
            Console.WriteLine("Entre un nombre : ");
            long.TryParse(Console.ReadLine(), out long nbTbd);
            bool reponse = PairImpair(nbTbd);
            Console.WriteLine(reponse);
            Console.ReadKey();
            #endregion

            #region Exo1_2
            List<long> listeNombres = new List<long> { 1, 5, 9, 4, 8, 12, 15, 20 };
            List<long> listePaire = new List<long>();
            listePaire = RetourPair(listeNombres);
            foreach (int item in listePaire)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
            #endregion

            Anagramme("Don't worry Be happy");

        }

        private static void Anagramme(string v)
        {
            
        }

        private static List<long> RetourPair(List<long> listeNombres)
        {
            List<long> estPair = new List<long>();
            foreach (int item in listeNombres)
            {
                if (item % 2 == 0)
                {
                    estPair.Add(item);
                }
            }
            return estPair;
        }

        public static bool PairImpair(long nbTbd)
        {
            long resultat = nbTbd % 2;
            if (resultat == 0)
            {
                return true;
            }
            else
            {
                return false;
            } 
        }
    }
}
